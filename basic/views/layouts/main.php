<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link href="/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/menu.css" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="menu">
    <ul id="navbar1" open="false" onclick="this.setAttribute('open', this.getAttribute('open') != 'true')">
        <li><a href="https:/google.com"><img id="logo" src="images/logo.png"></a></li>
        <li>
            <a class="nav-text"  href="#">Новости</a>
        </li>
        <li id="active">
            <a class="nav-text" href="/index.php">Рейтинг магазинов</a>
        </li>
        <li>
            <a class="nav-text" href="#">Про сайт</a>
        </li>
        <li>
            <button type="button" id="login" href="#"><b>Войти</b></button>
        </li>
    </ul>
</div>


<div class="container-fluid" style="background: sandybrown">
    <div class="row">
<div class="col-lg-8 col-md8 col-sm-8 col-xs-8">
</div>
</div>
    <?php

//    NavBar::begin([
//        'brandLabel' => '<IMG src="images/logo.png">',
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//            //'class' => 'menu-color'
//        ],
//    ]);
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => [
//            ['label' => 'Домашняя', 'url' => ['/site/index']],
//            ['label' => 'Про нас', 'url' => ['/site/about']],
//            ['label' => 'Обратная связь', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
//        ],
//    ]);
//    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; MaGazines <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="/dist/js/bootstrap.min.js"></script>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
